#include "result.h"

#include <cassert>
#include <string>

void no_arg_function_void() { }

void excepting_no_arg_function_void() { throw std::exception(); }

std::string no_arg_function_string() { return "Hello World"; }

std::string excepting_no_arg_function_string()
{
    throw std::exception();
    return "Hello World";
}

void two_arg_function_void(bool, bool) { }

bool two_arg_function_bool(bool, bool) { return false; }

int main()
{
    const auto result = result::run(&no_arg_function_void);
    static_assert((std::is_same_v<typeof(result), const Result<std::monostate>>));
    assert(result.is_ok());
    assert(result.ok().has_value());
    assert(!result.is_err());

    const auto result1 = result::run(&excepting_no_arg_function_void);
    assert(result1.is_err());
    assert(result1.err().has_value());
    assert(!result1.is_ok());

    const auto result2 = result::run(&no_arg_function_string);
    static_assert((std::is_same_v<typeof(result2), const Result<std::string>>));
    assert(result2.unwrap() == "Hello World");
    const auto map_result = result2.map<bool>(
        [](const std::string& content) { return content == "Hello World"; });
    assert(map_result.is_ok());
    assert(map_result.unwrap() == true);

    const auto result3 = result::run(&two_arg_function_void, true, true);
    static_assert(
        (std::is_same_v<typeof(result3), const Result<std::monostate>>));

    const auto result4 = result::run(&excepting_no_arg_function_string);
    const auto text = result4.unwrap_or_default();
    assert(text.empty());

    const auto result5 = result4
                             .and_then([]() -> Result<std::string> {
                                 return Result<std::string>::from_value("Hello World");
                             })
                             .and_then([]() -> Result<std::string> {
                                 return Result<std::string>::from_exception(std::exception());
                             })
                            .and_then([]() -> Result<std::string> {
                                  return result::run<std::string>([]() -> std::string {
                                      throw std::exception();
                                      return "Unreachable";
                                  });
                              });
    assert(result.is_err());

    const auto result6 = result5.or_else([]() -> Result<std::string> {
        return Result<std::string>::from_value("Test");
    });
    assert(result6.is_ok());
}
