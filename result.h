#ifndef EXCEPTION_RESULT_H
#define EXCEPTION_RESULT_H

#include <exception>
#include <functional>
#include <variant>
#include <cassert>
#include <iostream>

//
// Generic macro for cases that no template is available for
//

#define handle_exception(T, exception_code) \
    [&]() { \
        try { \
            return Result<T>::from_value(exception_code); \
        } catch (std::exception &exception) { \
            return Result<T>::from_exception(exception); \
        }; \
    }();

#define handle_void_exception(exception_code) \
    [&]() { \
        try { \
            { \
                exception_code; \
            } \
            return Result<std::monostate>::from_void(); \
        } catch (std::exception &exception) { \
            return Result<std::monostate>::from_exception(exception); \
        }; \
    }();

template<typename T>
class Result {
public:
    static auto from_exception(const std::exception &exception) -> Result<T> {
        Result<T> result;
        result.m_content = exception;
        return result;
    }
    static auto from_value(T &&value) -> Result<T> {
        Result<T> result;
        result.m_content = value;
        return result;
    }
    static auto from_void() -> Result<std::monostate> {
        return Result::from_value(std::monostate());
    }

    auto is_ok() const -> bool {
        return std::holds_alternative<T>(m_content);
    }

    auto is_err() const -> bool {
        return std::holds_alternative<std::exception>(m_content);
    }

    auto unwrap() const -> T {
        assert(is_ok());

        return std::get<T>(m_content);
    }

    auto expect(const std::string &error) const -> T {
        if (is_err()) {
            std::cerr << "expect() called on an error result: " << error << std::endl;
            std::abort();
        }

        return unwrap();
    }

    auto expect_err(const std::string &error) const -> T {
        if (is_ok()) {
            std::cerr << "expect_err() called on an ok result: " << error << std::endl;
            std::abort();
        }

        return unwrap_err();
    }

    auto unwrap_or(T other) const -> T {
        if (is_err()) {
            return other;
        }

        return unwrap();
    }

    auto unwrap_or_default() const -> T {
        if (is_err()) {
            return T();
        }

        return unwrap();
    }

    auto ok() const -> std::optional<T> {
        if (is_ok()) {
            return unwrap();
        }

        return std::nullopt;
    }

    auto err() const -> std::optional<std::exception> {
        if (is_err()) {
            return unwrap_err();
        }

        return std::nullopt;
    }

    auto unwrap_err() const -> std::exception {
        assert(std::holds_alternative<std::exception>(m_content));

        return std::get<std::exception>(m_content);
    }

    template <typename M>
    auto map(M (*function)(T)) const -> Result<M> {
        if (is_ok()) {
            return Result<M>::from_value(function(std::get<T>(m_content)));
        }

        return Result<M>::from_exception(std::get<std::exception>(m_content));
    }

    template <typename M>
    auto map(M (*function)(const T &)) const -> Result<M> {
        if (is_ok()) {
            return Result<M>::from_value(function(std::get<T>(m_content)));
        }

        return Result<M>::from_exception(std::get<std::exception>(m_content));
    }

    auto and_also(Result<T> other) const -> Result<T> {
        if (is_err()) {
            return *this;
        }

        return other;
    }

    auto and_then(Result<T> (*op)()) const -> Result<T> {
        if (is_err()) {
            return *this;
        }

        return op();
    }

    auto or_other(Result<T> other) const -> Result<T> {
        if (is_err()) {
            return other;
        }

        return *this;
    }

    auto or_else(Result<T> (*op)()) const -> Result<T> {
        if (is_err()) {
            return op();
        }

        return *this;
    }

private:
    std::variant<T, std::exception> m_content;
};

namespace result {
    //
    // Overloads for different number of function arguments
    //

    template <typename T>
    static Result<T> run(T (*function)()) {
        return handle_exception(T, function())
    }

    static Result<std::monostate> run(void (*function)()) {
        return handle_void_exception(function());
    }

    template <typename T, typename A1>
    static Result<T> run(T (*function)(A1), A1 arg1) {
        return handle_exception(T, function(arg1));
    }

    template <typename A1>
    static Result<std::monostate> run(void (*function)(A1), A1 arg1) {
        return handle_void_exception(function(arg1));
    }

    template <typename T, typename A1, typename A2>
    static Result<T> run(T (*function)(A1, A2), A1 arg1, A2 arg2) {
        return handle_exception(T, function(arg1, arg2));
    }

    template <typename A1, typename A2>
    static Result<std::monostate> run(void (*function)(A1, A2), A1 arg1, A2 arg2) {
        return handle_void_exception(function(arg1, arg2));
    }

    template <typename T, typename A1, typename A2, typename A3>
    static Result<T> run(T (*function)(A1, A2, A3), A1 arg1, A2 arg2, A3 arg3) {
        return handle_exception(T, function(arg1, arg2, arg3));
    }

    template <typename A1, typename A2, typename A3>
    static Result<std::monostate> run(void (*function)(A1, A2, A3), A1 arg1, A2 arg2, A3 arg3) {
        return handle_void_exception(function(arg1, arg2, arg3));
    }

    template <typename T, typename A1, typename A2, typename A3, typename A4>
    static Result<T> run(T (*function)(A1, A2, A3, A4), A1 arg1, A2 arg2, A3 arg3, A4 arg4) {
        return handle_exception(T, function(arg1, arg2, arg3, arg4));
    }

    template <typename A1, typename A2, typename A3, typename A4>
    Result<std::monostate> run(void (*function)(A1, A2, A3, A4), A1 arg1, A2 arg2, A3 arg3, A4 arg4) {
        return handle_void_exception(function(arg1, arg2, arg3, arg4));
    }

    template <typename T, typename A1, typename A2, typename A3, typename A4, typename A5>
    static Result<T> run(T (*function)(A1, A2, A3, A4, A5), A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5) {
        return handle_exception(T, function(arg1, arg2, arg3, arg4, arg5));
    }

    template <typename A1, typename A2, typename A3, typename A4, typename A5>
    static Result<std::monostate> run(void (*function)(A1, A2, A3, A4), A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5) {
        return handle_void_exception(function(arg1, arg2, arg3, arg4, arg5));
    }
}

#endif // EXCEPTION_RESULT_H
